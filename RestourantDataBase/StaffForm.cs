﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace RestourantDataBase
{
    public enum Status { Waiter, Administrator, Chief };

    public partial class StaffForm : Form
    {
        OleDbConnection dbConn;
        OleDbDataAdapter adapter;
        private string username;
        private Status userStatus;
        private DataTable staffDt;

        public StaffForm(OleDbConnection cn, string user, string status)
        {
            username = user;
            dbConn = cn;

            InitializeComponent();

            SetUserStatus(status);
            SetUserAccess();

        }

        private void SetUserStatus(string status)
        {
            switch (status)
            {
                case "Waiter":
                    userStatus = Status.Waiter;
                    break;
                case "Administrator":
                    userStatus = Status.Administrator;
                    break;
                case "Сhief":
                    userStatus = Status.Chief;
                    break;
            }

        }

        // Remove tabs depends on user permissions 
        private void SetUserAccess()
        {
            int tabCount = tabControl1.TabPages.Count;

            //for (int i = (int)userStatus + 1; i < tabCount; i++)
            //    tabControl1.Controls.Remove(tabControl1.TabPages[(int)userStatus + 1]);

            if (userStatus != Status.Chief)
                tabControl1.Controls.Remove((tabControl1.TabPages[tabCount - 1]));

            if (userStatus == Status.Waiter)
            {
                tabControl1.Controls.Remove((tabControl1.TabPages[tabCount - 2]));
                tabControl1.Controls.Remove((tabControl1.TabPages[tabCount - 3]));
            }

        }

        private void WaiterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (dbConn.State != ConnectionState.Closed)
                dbConn.Close();

            var loginForm = Application.OpenForms[0];
            loginForm.Show();
        }

        private void addDish_Click(object sender, EventArgs e)
        {

            if (dishesNum.Text.Equals(""))
                return;

            int dishNum = Convert.ToInt32(dishesNum.Text);

            if (dishNum == 0 || lvMenu.SelectedItems.Count == 0)
                return;

            var selectedItem = lvMenu.SelectedItems[0].SubItems;
            string dishName = selectedItem[0].Text;
            double dishPrice = Convert.ToDouble(selectedItem[1].Text);

            OleDbCommand cmd;

            try
            {
                dbConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }

            var item = lvOrder.FindItemWithText(dishName);
            bool isNewItem = true;
            int newNumber = dishNum;

            // If dish have already added
            if (item != null)
            {
                isNewItem = false;
                newNumber = Convert.ToInt32(item.SubItems[1].Text) + dishNum;
            }

            // Check the availability of the ingredients
            cmd = new OleDbCommand("Sp_Check_For_Ingredients", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@dishName", dishName));
            cmd.Parameters.Add(new OleDbParameter("@dishNum", dishNum));

            // Not enough ingredients
            OleDbDataReader reader = cmd.ExecuteReader();

            reader.Read();
            int res = Convert.ToInt32(reader[0].ToString());

            if (res != -1)
            {
                if (isNewItem)
                {
                    String[] temp = { dishName, dishNum.ToString(), (newNumber * dishPrice).ToString() };
                    lvOrder.Items.Add(new ListViewItem(temp));
                }
                else
                {
                    item.SubItems[1].Text = newNumber.ToString();
                    item.SubItems[2].Text = (newNumber * dishPrice).ToString();
                }
                tbTotalPrice.Text = (Convert.ToDouble(tbTotalPrice.Text) + dishNum * dishPrice).ToString();

                List<KeyValuePair<int, int>> list = new List<KeyValuePair<int, int>>();
                list.Add(new KeyValuePair<int, int>(res, Convert.ToInt32(reader[1].ToString())));

                if (dishNum != 1)
                    while (reader.Read())
                        list.Add(
                            new KeyValuePair<int, int>(
                                Convert.ToInt32(reader[0].ToString()),
                                Convert.ToInt32(reader[1].ToString())));

                cmd = new OleDbCommand("Sp_Subtract_Ingredients", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (KeyValuePair<int, int> i in list)
                {
                    cmd.Parameters.Add(new OleDbParameter("@ingredientId", i.Key));
                    cmd.Parameters.Add(new OleDbParameter("@num", i.Value));
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
            else
            {
                MessageBox.Show("Not enough ingredients", "Order", MessageBoxButtons.OK);
            }
            dbConn.Close();

            if (userStatus != Status.Waiter)
                updateStockList();
        }

        private void applyOrder_Click(object sender, EventArgs e)
        {
            //If order list is empty
            if (lvOrder.Items.Count == 0)
            {
                MessageBox.Show("The order is empty", "Order", MessageBoxButtons.OK);
                return;
            }

            try
            {
                dbConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }

            // Add new order
            OleDbCommand cmd = new OleDbCommand("Sp_Add_Order", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@userName", username));
            OleDbDataReader reader = cmd.ExecuteReader();

             reader.Read();
            int staffId = Convert.ToInt32(reader[0].ToString());
            int orderId = Convert.ToInt32(reader[1].ToString());

            // Add new order components
            cmd = new OleDbCommand("Sp_Add_Order_Component", this.dbConn);
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (ListViewItem item in this.lvOrder.Items)
            {
                cmd.Parameters.Add(new OleDbParameter("@orderId", orderId));
                cmd.Parameters.Add(new OleDbParameter("@dishName", item.SubItems[0].Text));
                cmd.Parameters.Add(new OleDbParameter("@num", Convert.ToInt32(item.SubItems[1].Text)));
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }

            //Get staff id from arhive
            cmd = new OleDbCommand("Sp_Get_Archive_Staff_Id", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@staffId", staffId));
            staffId = (int)cmd.ExecuteScalar();
            cmd.Parameters.Clear();

            //Add order to archive
            cmd = new OleDbCommand("Sp_Add_Order_To_Archive", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@staffId", staffId));
            cmd.Parameters.Add(new OleDbParameter("@price", Convert.ToDouble(tbTotalPrice.Text)));
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            //Add order price
            cmd = new OleDbCommand("Sp_Add_OrderPrice", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@orderId", orderId));
            cmd.Parameters.Add(new OleDbParameter("@price", Convert.ToDouble(tbTotalPrice.Text)));
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            MessageBox.Show("The order successfully applied!", "Order", MessageBoxButtons.OK);
            ClearDishOrderFields();

            dbConn.Close();
        }

        private void ClearDishOrderFields()
        {
            lvOrder.Items.Clear();
            lvOrder.Update();
            tbTotalPrice.Text="0";
            dishesNum.Clear();
            lvMenu.SelectedItems.Clear();
        }

        private void ClearEmployeeInfoFields()
        {
            tbEmployeeAge.Clear();
            tbEmployeeName.Clear();
            tbEmployeeSurname.Clear();
            cbName.Items.Clear();
            cbName.Text = "";
        }

        private void ClearEmployeeFilelds()
        {
            ClearEmployeeInfoFields();
            cbPosition.Text = "";
        }

        private void StaffForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                ///
                /// Dish Order Tab Page
                ///  

                // Fill Menu ListView
                string sqlSelectDishes = "SELECT dishName, price FROM Dishes";
                OleDbCommand cmd = new OleDbCommand(sqlSelectDishes, dbConn);
                DataTable dt = new DataTable();
                adapter = new OleDbDataAdapter(cmd);
                adapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    String[] temp = { row[0].ToString(), row[1].ToString() };
                    ListViewItem item = new ListViewItem(temp);
                    lvMenu.Items.Add(item);
                }
                lvMenu.Sort();
                dt.Clear();

                ///
                /// Ingredients Order Tab Page
                ///  
                updateStockList();

                ///
                /// Manage Staff Tab Page
                /// 

                for (Status i = 0; i < userStatus; i++)
                {
                    cbPosition.Items.Add(i.ToString());
                    cbNewEmployeePos.Items.Add(i.ToString());
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void updateStockList()
        {
            try
            {
                string sqlSelectStockInfo = "SELECT Ingredients.ingredientName, number FROM Stock " +
                    "JOIN Ingredients ON Ingredients.ingredientId = Stock.ingredientId " +
                    "ORDER BY number ASC";
                OleDbCommand cmd = new OleDbCommand(sqlSelectStockInfo, dbConn);
                adapter = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                lvStock.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    String[] temp = { row[0].ToString(), row[1].ToString() };
                    ListViewItem item = new ListViewItem(temp);
                    lvStock.Items.Add(item);
                }
                dt.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dishesNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Pressed digit or Backspace
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == '\b'))
            {
                e.Handled = true;
            }
        }

        private void tbIngredientNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Pressed digit or Backspace
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == '\b'))
            {
                e.Handled = true;
            }
        }

        private void addIngredientToOrder_Click(object sender, EventArgs e)
        {
            if (tbIngredientNum.Text.Equals(""))
                return;

            try
            {
                dbConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }

            var selectedItem = lvStock.SelectedItems[0].SubItems;
            string ingredientName = selectedItem[0].Text;
            float ingredientNum = (float)Convert.ToDouble(tbIngredientNum.Text);


            var item = lvIngredientOrder.FindItemWithText(ingredientName);
            bool isNewItem = true;
            float newNumber = ingredientNum;

            // If dish have already added
            if (item != null)
            {
                isNewItem = false;
                newNumber = Convert.ToInt32(item.SubItems[1].Text) + ingredientNum;
            }

            if (isNewItem)
            {
                item = new ListViewItem();
                item.SubItems[0].Text = ingredientName;
                item.SubItems.Add(newNumber.ToString());
                lvIngredientOrder.Items.Add(item);
            }
            else
            {
                item.SubItems[1].Text = newNumber.ToString();
            }

            dbConn.Close();
        }

        private void applyIngredientOrder_Click(object sender, EventArgs e)
        {
            // If order list is empty
            if (lvIngredientOrder.Items.Count == 0)
            {
                MessageBox.Show("The order is empty", "Order", MessageBoxButtons.OK);
                return;
            }

            try
            {
                dbConn.Open();

                OleDbCommand cmd = new OleDbCommand("Sp_Add_Ingredients", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (ListViewItem item in lvIngredientOrder.Items)
                {
                    cmd.Parameters.Add(new OleDbParameter("@name", item.SubItems[0].Text));
                    cmd.Parameters.Add(new OleDbParameter("@num", (float)Convert.ToDouble(item.SubItems[1].Text)));
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }

            MessageBox.Show("The order successfully applied!", "Order", MessageBoxButtons.OK);

            ClearIngredientOrderFields();
            dbConn.Close();
        }

        private void ClearIngredientOrderFields()
        {
            lvIngredientOrder.Items.Clear();
            lvIngredientOrder.Update();
            updateStockList();
            tbIngredientNum.Clear();
        }

        private void cbPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbName.Enabled = true;
            demoteButton.Visible = false;
            promoteButton.Visible = false;
            dismissButton.Visible = false;


            ClearEmployeeInfoFields();
            try
            {
                dbConn.Open();
                updatePositionNames();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        // Update combo box position names
        private void updatePositionNames()
        {
            OleDbCommand cmd = new OleDbCommand("Sp_Get_Staff_By_Position", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new OleDbParameter("@positionName", cbPosition.SelectedItem.ToString()));

            staffDt = new DataTable();
            adapter = new OleDbDataAdapter(cmd);
            adapter.Fill(staffDt);

            foreach (DataRow row in staffDt.Rows)
            {
                cbName.Items.Add(row[1].ToString());
            }
        }

        private void cbName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (staffDt.IsInitialized)
            {
                foreach (DataRow row in staffDt.Rows)
                {
                    if (row[1].ToString().Equals(cbName.SelectedItem.ToString()))
                    {
                        tbEmployeeName.Text = row[0].ToString();
                        tbEmployeeSurname.Text = row[1].ToString();
                        tbEmployeeAge.Text = row[2].ToString().Substring(0, 10);
                        break;
                    }
                }
                // Hide Demote button, if waiter selected
                demoteButton.Visible = cbPosition.SelectedItem.ToString() != Status.Waiter.ToString();

                // Show Promote button, if waiter selected
                promoteButton.Visible = cbPosition.SelectedItem.ToString() == Status.Waiter.ToString();

                // Show Dismiss button, if employee selected
                dismissButton.Visible = true;
            }
        }

        private void demoteButton_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();

                OleDbCommand cmd = new OleDbCommand("Sp_Demote_Employee", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new OleDbParameter("@surname", tbEmployeeSurname.Text));
                cmd.ExecuteNonQuery();

                MessageBox.Show("Complete! Now " + tbEmployeeName.Text +
                    " " + tbEmployeeSurname.Text + " is a Waiter!", "Staff", MessageBoxButtons.OK);

                ClearEmployeeFilelds();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }

        }

        private void promoteButton_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();

                OleDbCommand cmd = new OleDbCommand("Sp_promote_employee", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new OleDbParameter("@surname", tbEmployeeSurname.Text));
                cmd.ExecuteNonQuery();

                MessageBox.Show("Complete! Now " + tbEmployeeName.Text +
                    " " + tbEmployeeSurname.Text + " is an Administrator!", "Staff", MessageBoxButtons.OK);

                ClearEmployeeFilelds();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void SetNewEmployeeVisible(bool visible)
        {
            cbNewEmployeePos.Visible = visible;
            lNewEmployeePos.Visible = visible;

            tbNewEmployeeName.Visible = visible;
            lNewEmployeeName.Visible = visible;

            tbNewEmployeeSurname.Visible = visible;
            lNewEmployeeSurname.Visible = visible;

            tbNewEmployeeAge.Visible = visible;
            lNewEmployeeAge.Visible = visible;

            applyNewEmployee.Visible = visible;
        }

        private void newEmployeeButton_Click(object sender, EventArgs e)
        {
            SetNewEmployeeVisible(true);
        }

        private void applyNewEmployee_Click(object sender, EventArgs e)
        {

            try
            {
                dbConn.Open();

                OleDbCommand cmd = new OleDbCommand("Sp_Add_Employee", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new OleDbParameter("@name", tbNewEmployeeName.Text));
                cmd.Parameters.Add(new OleDbParameter("@surname", tbNewEmployeeSurname.Text));
                cmd.Parameters.Add(new OleDbParameter("@birthday", Convert.ToDateTime(tbNewEmployeeAge.Text).Date));
                cmd.Parameters.Add(new OleDbParameter("@positionName", cbNewEmployeePos.SelectedItem.ToString()));
                cmd.Parameters.Add(new OleDbParameter("@username",
                    (tbNewEmployeeSurname.Text + "." + tbNewEmployeeName.Text).ToLower()));
                cmd.ExecuteNonQuery();


                MessageBox.Show("Complete! Now " + tbNewEmployeeName.Text +
                    " " + tbNewEmployeeSurname.Text + " is a new " + cbNewEmployeePos.SelectedItem.ToString(),
                    "Staff", MessageBoxButtons.OK);

                SetNewEmployeeVisible(false);
                tbNewEmployeeName.Clear();
                tbNewEmployeeSurname.Clear();
                tbNewEmployeeAge.Clear();
                 
                updatePositionNames();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void dismissButton_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();

                OleDbCommand cmd = new OleDbCommand("Sp_Dismiss_Employee", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new OleDbParameter("@name", tbEmployeeName.Text));
                cmd.Parameters.Add(new OleDbParameter("@surname", tbEmployeeSurname.Text));
                cmd.Parameters.Add(new OleDbParameter("@birthday", Convert.ToDateTime(tbEmployeeAge.Text).Date));
                cmd.ExecuteNonQuery();

                MessageBox.Show("Complete! " + tbEmployeeName.Text + " " + tbEmployeeSurname.Text + " dismissed!",
                    "Staff", MessageBoxButtons.OK);

                ClearEmployeeFilelds();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void showBestWorkerButton_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();
                dSet = new DataSet();
   
                OleDbCommand cmd = new OleDbCommand("Sp_Best_Day_Worker", dbConn);
                cmd.CommandType = CommandType.StoredProcedure; ;
                cmd.Parameters.Add(new OleDbParameter("@date", tbBestWorkerDate.Text));

                bindDataReports("Staff", cmd);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void bindDataReports(string tableName, OleDbCommand cmd)
        {
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);

            DataTable tableCustomers = new DataTable(tableName);
            dSet.Tables.Add(tableCustomers);
            dgReports.SetDataBinding(dSet, tableName);
            dAdapter.Fill(dSet, tableName);
            dgReports.DataMember = tableName;
        }

        private void showUnclaimedIngrButton_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();
                dSet = new DataSet();

                OleDbCommand cmd = new OleDbCommand("Sp_Unclaimed_Ingredients", dbConn);
                cmd.CommandType = CommandType.StoredProcedure; ;
                cmd.Parameters.Add(new OleDbParameter("@date1", tbDateStart.Text));
                cmd.Parameters.Add(new OleDbParameter("@date2", tbDateEnd.Text));

                bindDataReports("Ingredients", cmd);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }

        private void showAvgIngredientsButtons_Click(object sender, EventArgs e)
        {
            try
            {
                dbConn.Open();
                dSet = new DataSet();

                OleDbCommand cmd = new OleDbCommand("Sp_Average_Ingedient_Consuming", dbConn);
                cmd.CommandType = CommandType.StoredProcedure; ;
                cmd.Parameters.Add(new OleDbParameter("@date1", tbDateStart.Text));
                cmd.Parameters.Add(new OleDbParameter("@date2", tbDateEnd.Text));

                bindDataReports("Ingredients", cmd);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }
    }
}

