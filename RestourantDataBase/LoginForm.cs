﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace RestourantDataBase
{
    public partial class LoginForm : Form
    {
        string strConn;
        public LoginForm()
        {
            InitializeComponent();

        }

        private void bLogin(object sender, EventArgs e)
        {

            // strConn = "Provider=SQLNCLI11;Data Source=DESKTOP-A1F41SE\\SQLEXPRESS;" +
            //     "Integrated Security=SSPI;Initial Catalog=restourant_db";
            ///    dbConn.ConnectionString = strConn;


            try
            {
                dbConn.Open();


                OleDbCommand cmd = new OleDbCommand("Sp_User_Auth", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new OleDbParameter("@login", tbUsername.Text));
                cmd.Parameters.Add(new OleDbParameter("@password", tbPassword.Text));

                var posName = cmd.ExecuteScalar();

                // If wrong login info
                if (posName == null)
                {
                    MessageBox.Show("Authorisation error!", "Connection", MessageBoxButtons.OK);
                    return;
                }

                Form newForm = new StaffForm(dbConn, tbUsername.Text.ToLower(), posName.ToString());
                newForm.Show();
                Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK);
                return;
            }
            finally
            {
                dbConn.Close();
            }
        }
    }
}
