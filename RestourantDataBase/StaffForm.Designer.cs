﻿namespace RestourantDataBase
{
    partial class StaffForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.makeOrderTab = new System.Windows.Forms.TabPage();
            this.applyOrder = new System.Windows.Forms.Button();
            this.tbTotalPrice = new System.Windows.Forms.TextBox();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.lvOrder = new System.Windows.Forms.ListView();
            this.dishNameOrder = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishNumOrder = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishPriceOrder = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.addDish = new System.Windows.Forms.Button();
            this.labelNumber = new System.Windows.Forms.Label();
            this.dishesNum = new System.Windows.Forms.TextBox();
            this.labelOrder = new System.Windows.Forms.Label();
            this.labelMenu = new System.Windows.Forms.Label();
            this.lvMenu = new System.Windows.Forms.ListView();
            this.dishNameMenu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dishPriceMenu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.orderIngredientsTab = new System.Windows.Forms.TabPage();
            this.applyIngredientOrder = new System.Windows.Forms.Button();
            this.lvIngredientOrder = new System.Windows.Forms.ListView();
            this.orderIngredientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.orderIngredientNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.addIngredientToOrder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbIngredientNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lvStock = new System.Windows.Forms.ListView();
            this.stockIngredientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stockIngredientNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.manageStaffTab = new System.Windows.Forms.TabPage();
            this.tbNewEmployeeName = new System.Windows.Forms.TextBox();
            this.applyNewEmployee = new System.Windows.Forms.Button();
            this.newEmployeeButton = new System.Windows.Forms.Button();
            this.dismissButton = new System.Windows.Forms.Button();
            this.demoteButton = new System.Windows.Forms.Button();
            this.promoteButton = new System.Windows.Forms.Button();
            this.tbEmployeeAge = new System.Windows.Forms.TextBox();
            this.lNewEmployeeAge = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbNewEmployeeAge = new System.Windows.Forms.TextBox();
            this.tbEmployeeSurname = new System.Windows.Forms.TextBox();
            this.lNewEmployeeSurname = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNewEmployeeSurname = new System.Windows.Forms.TextBox();
            this.tbEmployeeName = new System.Windows.Forms.TextBox();
            this.lNewEmployeeName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lNewEmployeePos = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbName = new System.Windows.Forms.ComboBox();
            this.cbNewEmployeePos = new System.Windows.Forms.ComboBox();
            this.cbPosition = new System.Windows.Forms.ComboBox();
            this.reportsTabPage = new System.Windows.Forms.TabPage();
            this.dSet = new System.Data.DataSet();
            this.showBestWorkerButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.tbDateStart = new System.Windows.Forms.TextBox();
            this.tbDateEnd = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.showUnclaimedIngrButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.showAvgIngredientsButtons = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.dgReports = new System.Windows.Forms.DataGrid();
            this.tbBestWorkerDate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.makeOrderTab.SuspendLayout();
            this.orderIngredientsTab.SuspendLayout();
            this.manageStaffTab.SuspendLayout();
            this.reportsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgReports)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.makeOrderTab);
            this.tabControl1.Controls.Add(this.orderIngredientsTab);
            this.tabControl1.Controls.Add(this.manageStaffTab);
            this.tabControl1.Controls.Add(this.reportsTabPage);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 449);
            this.tabControl1.TabIndex = 0;
            // 
            // makeOrderTab
            // 
            this.makeOrderTab.Controls.Add(this.applyOrder);
            this.makeOrderTab.Controls.Add(this.tbTotalPrice);
            this.makeOrderTab.Controls.Add(this.labelTotalPrice);
            this.makeOrderTab.Controls.Add(this.lvOrder);
            this.makeOrderTab.Controls.Add(this.addDish);
            this.makeOrderTab.Controls.Add(this.labelNumber);
            this.makeOrderTab.Controls.Add(this.dishesNum);
            this.makeOrderTab.Controls.Add(this.labelOrder);
            this.makeOrderTab.Controls.Add(this.labelMenu);
            this.makeOrderTab.Controls.Add(this.lvMenu);
            this.makeOrderTab.Location = new System.Drawing.Point(4, 25);
            this.makeOrderTab.Name = "makeOrderTab";
            this.makeOrderTab.Padding = new System.Windows.Forms.Padding(3);
            this.makeOrderTab.Size = new System.Drawing.Size(792, 420);
            this.makeOrderTab.TabIndex = 0;
            this.makeOrderTab.Text = "Make Order";
            this.makeOrderTab.UseVisualStyleBackColor = true;
            // 
            // applyOrder
            // 
            this.applyOrder.BackColor = System.Drawing.Color.SpringGreen;
            this.applyOrder.Location = new System.Drawing.Point(689, 379);
            this.applyOrder.Name = "applyOrder";
            this.applyOrder.Size = new System.Drawing.Size(75, 28);
            this.applyOrder.TabIndex = 18;
            this.applyOrder.Text = "Apply";
            this.applyOrder.UseVisualStyleBackColor = false;
            this.applyOrder.Click += new System.EventHandler(this.applyOrder_Click);
            // 
            // tbTotalPrice
            // 
            this.tbTotalPrice.Location = new System.Drawing.Point(517, 382);
            this.tbTotalPrice.Name = "tbTotalPrice";
            this.tbTotalPrice.ReadOnly = true;
            this.tbTotalPrice.Size = new System.Drawing.Size(100, 22);
            this.tbTotalPrice.TabIndex = 17;
            this.tbTotalPrice.Text = "0";
            this.tbTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Location = new System.Drawing.Point(471, 382);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(40, 17);
            this.labelTotalPrice.TabIndex = 16;
            this.labelTotalPrice.Text = "Total";
            // 
            // lvOrder
            // 
            this.lvOrder.AutoArrange = false;
            this.lvOrder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dishNameOrder,
            this.dishNumOrder,
            this.dishPriceOrder});
            this.lvOrder.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvOrder.GridLines = true;
            this.lvOrder.Location = new System.Drawing.Point(471, 37);
            this.lvOrder.MultiSelect = false;
            this.lvOrder.Name = "lvOrder";
            this.lvOrder.Size = new System.Drawing.Size(293, 314);
            this.lvOrder.TabIndex = 15;
            this.lvOrder.UseCompatibleStateImageBehavior = false;
            this.lvOrder.View = System.Windows.Forms.View.Details;
            // 
            // dishNameOrder
            // 
            this.dishNameOrder.Text = "Name";
            this.dishNameOrder.Width = 85;
            // 
            // dishNumOrder
            // 
            this.dishNumOrder.Text = "Number";
            // 
            // dishPriceOrder
            // 
            this.dishPriceOrder.Text = "Price";
            // 
            // addDish
            // 
            this.addDish.Location = new System.Drawing.Point(334, 181);
            this.addDish.Name = "addDish";
            this.addDish.Size = new System.Drawing.Size(100, 23);
            this.addDish.TabIndex = 14;
            this.addDish.Text = "Add";
            this.addDish.UseVisualStyleBackColor = true;
            this.addDish.Click += new System.EventHandler(this.addDish_Click);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(331, 108);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(58, 17);
            this.labelNumber.TabIndex = 13;
            this.labelNumber.Text = "Number";
            // 
            // dishesNum
            // 
            this.dishesNum.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dishesNum.Location = new System.Drawing.Point(334, 128);
            this.dishesNum.Name = "dishesNum";
            this.dishesNum.Size = new System.Drawing.Size(100, 22);
            this.dishesNum.TabIndex = 12;
            this.dishesNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dishesNum_KeyPress);
            // 
            // labelOrder
            // 
            this.labelOrder.AutoSize = true;
            this.labelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOrder.Location = new System.Drawing.Point(470, 14);
            this.labelOrder.Name = "labelOrder";
            this.labelOrder.Size = new System.Drawing.Size(57, 20);
            this.labelOrder.TabIndex = 10;
            this.labelOrder.Text = "Order";
            // 
            // labelMenu
            // 
            this.labelMenu.AutoSize = true;
            this.labelMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMenu.Location = new System.Drawing.Point(29, 14);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(54, 20);
            this.labelMenu.TabIndex = 11;
            this.labelMenu.Text = "Menu";
            // 
            // lvMenu
            // 
            this.lvMenu.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvMenu.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lvMenu.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dishNameMenu,
            this.dishPriceMenu});
            this.lvMenu.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvMenu.FullRowSelect = true;
            this.lvMenu.GridLines = true;
            this.lvMenu.HideSelection = false;
            this.lvMenu.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.lvMenu.Location = new System.Drawing.Point(33, 37);
            this.lvMenu.MultiSelect = false;
            this.lvMenu.Name = "lvMenu";
            this.lvMenu.Size = new System.Drawing.Size(280, 363);
            this.lvMenu.TabIndex = 9;
            this.lvMenu.UseCompatibleStateImageBehavior = false;
            this.lvMenu.View = System.Windows.Forms.View.Details;
            // 
            // dishNameMenu
            // 
            this.dishNameMenu.Text = "Name";
            this.dishNameMenu.Width = 140;
            // 
            // dishPriceMenu
            // 
            this.dishPriceMenu.Text = "Price";
            this.dishPriceMenu.Width = 70;
            // 
            // orderIngredientsTab
            // 
            this.orderIngredientsTab.Controls.Add(this.applyIngredientOrder);
            this.orderIngredientsTab.Controls.Add(this.lvIngredientOrder);
            this.orderIngredientsTab.Controls.Add(this.addIngredientToOrder);
            this.orderIngredientsTab.Controls.Add(this.label2);
            this.orderIngredientsTab.Controls.Add(this.tbIngredientNum);
            this.orderIngredientsTab.Controls.Add(this.label3);
            this.orderIngredientsTab.Controls.Add(this.label4);
            this.orderIngredientsTab.Controls.Add(this.lvStock);
            this.orderIngredientsTab.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.orderIngredientsTab.Location = new System.Drawing.Point(4, 25);
            this.orderIngredientsTab.Name = "orderIngredientsTab";
            this.orderIngredientsTab.Padding = new System.Windows.Forms.Padding(3);
            this.orderIngredientsTab.Size = new System.Drawing.Size(792, 420);
            this.orderIngredientsTab.TabIndex = 1;
            this.orderIngredientsTab.Text = "Order Ingredients";
            this.orderIngredientsTab.UseVisualStyleBackColor = true;
            // 
            // applyIngredientOrder
            // 
            this.applyIngredientOrder.BackColor = System.Drawing.Color.SpringGreen;
            this.applyIngredientOrder.Location = new System.Drawing.Point(559, 372);
            this.applyIngredientOrder.Name = "applyIngredientOrder";
            this.applyIngredientOrder.Size = new System.Drawing.Size(134, 28);
            this.applyIngredientOrder.TabIndex = 28;
            this.applyIngredientOrder.Text = "Apply";
            this.applyIngredientOrder.UseVisualStyleBackColor = false;
            this.applyIngredientOrder.Click += new System.EventHandler(this.applyIngredientOrder_Click);
            // 
            // lvIngredientOrder
            // 
            this.lvIngredientOrder.AutoArrange = false;
            this.lvIngredientOrder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.orderIngredientName,
            this.orderIngredientNum});
            this.lvIngredientOrder.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvIngredientOrder.GridLines = true;
            this.lvIngredientOrder.Location = new System.Drawing.Point(471, 37);
            this.lvIngredientOrder.MultiSelect = false;
            this.lvIngredientOrder.Name = "lvIngredientOrder";
            this.lvIngredientOrder.Size = new System.Drawing.Size(293, 314);
            this.lvIngredientOrder.TabIndex = 25;
            this.lvIngredientOrder.UseCompatibleStateImageBehavior = false;
            this.lvIngredientOrder.View = System.Windows.Forms.View.Details;
            // 
            // orderIngredientName
            // 
            this.orderIngredientName.Text = "Name";
            this.orderIngredientName.Width = 85;
            // 
            // orderIngredientNum
            // 
            this.orderIngredientNum.Text = "Number";
            // 
            // addIngredientToOrder
            // 
            this.addIngredientToOrder.Location = new System.Drawing.Point(334, 181);
            this.addIngredientToOrder.Name = "addIngredientToOrder";
            this.addIngredientToOrder.Size = new System.Drawing.Size(100, 23);
            this.addIngredientToOrder.TabIndex = 24;
            this.addIngredientToOrder.Text = "Add";
            this.addIngredientToOrder.UseVisualStyleBackColor = true;
            this.addIngredientToOrder.Click += new System.EventHandler(this.addIngredientToOrder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 23;
            this.label2.Text = "Number";
            // 
            // tbIngredientNum
            // 
            this.tbIngredientNum.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tbIngredientNum.Location = new System.Drawing.Point(334, 128);
            this.tbIngredientNum.Name = "tbIngredientNum";
            this.tbIngredientNum.Size = new System.Drawing.Size(100, 22);
            this.tbIngredientNum.TabIndex = 22;
            this.tbIngredientNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIngredientNum_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(470, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 20;
            this.label3.Text = "Order";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(29, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "Stock";
            // 
            // lvStock
            // 
            this.lvStock.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvStock.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lvStock.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.stockIngredientName,
            this.stockIngredientNum});
            this.lvStock.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvStock.FullRowSelect = true;
            this.lvStock.GridLines = true;
            this.lvStock.HideSelection = false;
            this.lvStock.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lvStock.Location = new System.Drawing.Point(33, 37);
            this.lvStock.MultiSelect = false;
            this.lvStock.Name = "lvStock";
            this.lvStock.Size = new System.Drawing.Size(280, 363);
            this.lvStock.TabIndex = 19;
            this.lvStock.UseCompatibleStateImageBehavior = false;
            this.lvStock.View = System.Windows.Forms.View.Details;
            // 
            // stockIngredientName
            // 
            this.stockIngredientName.Text = "Name";
            this.stockIngredientName.Width = 140;
            // 
            // stockIngredientNum
            // 
            this.stockIngredientNum.Text = "Number";
            this.stockIngredientNum.Width = 70;
            // 
            // manageStaffTab
            // 
            this.manageStaffTab.Controls.Add(this.tbNewEmployeeName);
            this.manageStaffTab.Controls.Add(this.applyNewEmployee);
            this.manageStaffTab.Controls.Add(this.newEmployeeButton);
            this.manageStaffTab.Controls.Add(this.dismissButton);
            this.manageStaffTab.Controls.Add(this.demoteButton);
            this.manageStaffTab.Controls.Add(this.promoteButton);
            this.manageStaffTab.Controls.Add(this.tbEmployeeAge);
            this.manageStaffTab.Controls.Add(this.lNewEmployeeAge);
            this.manageStaffTab.Controls.Add(this.label8);
            this.manageStaffTab.Controls.Add(this.tbNewEmployeeAge);
            this.manageStaffTab.Controls.Add(this.tbEmployeeSurname);
            this.manageStaffTab.Controls.Add(this.lNewEmployeeSurname);
            this.manageStaffTab.Controls.Add(this.label7);
            this.manageStaffTab.Controls.Add(this.tbNewEmployeeSurname);
            this.manageStaffTab.Controls.Add(this.tbEmployeeName);
            this.manageStaffTab.Controls.Add(this.lNewEmployeeName);
            this.manageStaffTab.Controls.Add(this.label6);
            this.manageStaffTab.Controls.Add(this.label5);
            this.manageStaffTab.Controls.Add(this.lNewEmployeePos);
            this.manageStaffTab.Controls.Add(this.label1);
            this.manageStaffTab.Controls.Add(this.cbName);
            this.manageStaffTab.Controls.Add(this.cbNewEmployeePos);
            this.manageStaffTab.Controls.Add(this.cbPosition);
            this.manageStaffTab.Location = new System.Drawing.Point(4, 25);
            this.manageStaffTab.Name = "manageStaffTab";
            this.manageStaffTab.Size = new System.Drawing.Size(792, 420);
            this.manageStaffTab.TabIndex = 2;
            this.manageStaffTab.Text = "Manage Staff";
            this.manageStaffTab.UseVisualStyleBackColor = true;
            // 
            // tbNewEmployeeName
            // 
            this.tbNewEmployeeName.Location = new System.Drawing.Point(543, 146);
            this.tbNewEmployeeName.Name = "tbNewEmployeeName";
            this.tbNewEmployeeName.Size = new System.Drawing.Size(217, 22);
            this.tbNewEmployeeName.TabIndex = 5;
            this.tbNewEmployeeName.Visible = false;
            // 
            // applyNewEmployee
            // 
            this.applyNewEmployee.Location = new System.Drawing.Point(602, 345);
            this.applyNewEmployee.Name = "applyNewEmployee";
            this.applyNewEmployee.Size = new System.Drawing.Size(115, 35);
            this.applyNewEmployee.TabIndex = 4;
            this.applyNewEmployee.Text = "Apply";
            this.applyNewEmployee.UseVisualStyleBackColor = true;
            this.applyNewEmployee.Visible = false;
            this.applyNewEmployee.Click += new System.EventHandler(this.applyNewEmployee_Click);
            // 
            // newEmployeeButton
            // 
            this.newEmployeeButton.Location = new System.Drawing.Point(602, 25);
            this.newEmployeeButton.Name = "newEmployeeButton";
            this.newEmployeeButton.Size = new System.Drawing.Size(115, 35);
            this.newEmployeeButton.TabIndex = 4;
            this.newEmployeeButton.Text = "New employee";
            this.newEmployeeButton.UseVisualStyleBackColor = true;
            this.newEmployeeButton.Click += new System.EventHandler(this.newEmployeeButton_Click);
            // 
            // dismissButton
            // 
            this.dismissButton.Location = new System.Drawing.Point(255, 350);
            this.dismissButton.Name = "dismissButton";
            this.dismissButton.Size = new System.Drawing.Size(98, 30);
            this.dismissButton.TabIndex = 3;
            this.dismissButton.Text = "Dismiss";
            this.dismissButton.UseVisualStyleBackColor = true;
            this.dismissButton.Visible = false;
            this.dismissButton.Click += new System.EventHandler(this.dismissButton_Click);
            // 
            // demoteButton
            // 
            this.demoteButton.Location = new System.Drawing.Point(151, 350);
            this.demoteButton.Name = "demoteButton";
            this.demoteButton.Size = new System.Drawing.Size(98, 30);
            this.demoteButton.TabIndex = 3;
            this.demoteButton.Text = "Demote";
            this.demoteButton.UseVisualStyleBackColor = true;
            this.demoteButton.Visible = false;
            this.demoteButton.Click += new System.EventHandler(this.demoteButton_Click);
            // 
            // promoteButton
            // 
            this.promoteButton.Location = new System.Drawing.Point(48, 350);
            this.promoteButton.Name = "promoteButton";
            this.promoteButton.Size = new System.Drawing.Size(98, 30);
            this.promoteButton.TabIndex = 3;
            this.promoteButton.Text = "Promote";
            this.promoteButton.UseVisualStyleBackColor = true;
            this.promoteButton.Visible = false;
            this.promoteButton.Click += new System.EventHandler(this.promoteButton_Click);
            // 
            // tbEmployeeAge
            // 
            this.tbEmployeeAge.Location = new System.Drawing.Point(135, 272);
            this.tbEmployeeAge.Name = "tbEmployeeAge";
            this.tbEmployeeAge.ReadOnly = true;
            this.tbEmployeeAge.Size = new System.Drawing.Size(217, 22);
            this.tbEmployeeAge.TabIndex = 2;
            // 
            // lNewEmployeeAge
            // 
            this.lNewEmployeeAge.Location = new System.Drawing.Point(453, 254);
            this.lNewEmployeeAge.Name = "lNewEmployeeAge";
            this.lNewEmployeeAge.Size = new System.Drawing.Size(65, 17);
            this.lNewEmployeeAge.TabIndex = 1;
            this.lNewEmployeeAge.Text = "Birthday";
            this.lNewEmployeeAge.Visible = false;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(45, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Birthday";
            // 
            // tbNewEmployeeAge
            // 
            this.tbNewEmployeeAge.Location = new System.Drawing.Point(543, 254);
            this.tbNewEmployeeAge.Name = "tbNewEmployeeAge";
            this.tbNewEmployeeAge.Size = new System.Drawing.Size(217, 22);
            this.tbNewEmployeeAge.TabIndex = 2;
            this.tbNewEmployeeAge.Visible = false;
            // 
            // tbEmployeeSurname
            // 
            this.tbEmployeeSurname.Location = new System.Drawing.Point(135, 220);
            this.tbEmployeeSurname.Name = "tbEmployeeSurname";
            this.tbEmployeeSurname.ReadOnly = true;
            this.tbEmployeeSurname.Size = new System.Drawing.Size(217, 22);
            this.tbEmployeeSurname.TabIndex = 2;
            // 
            // lNewEmployeeSurname
            // 
            this.lNewEmployeeSurname.AutoSize = true;
            this.lNewEmployeeSurname.Location = new System.Drawing.Point(453, 198);
            this.lNewEmployeeSurname.Name = "lNewEmployeeSurname";
            this.lNewEmployeeSurname.Size = new System.Drawing.Size(65, 17);
            this.lNewEmployeeSurname.TabIndex = 1;
            this.lNewEmployeeSurname.Text = "Surname";
            this.lNewEmployeeSurname.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Surname";
            // 
            // tbNewEmployeeSurname
            // 
            this.tbNewEmployeeSurname.Location = new System.Drawing.Point(543, 198);
            this.tbNewEmployeeSurname.Name = "tbNewEmployeeSurname";
            this.tbNewEmployeeSurname.Size = new System.Drawing.Size(217, 22);
            this.tbNewEmployeeSurname.TabIndex = 2;
            this.tbNewEmployeeSurname.Visible = false;
            // 
            // tbEmployeeName
            // 
            this.tbEmployeeName.Location = new System.Drawing.Point(135, 164);
            this.tbEmployeeName.Name = "tbEmployeeName";
            this.tbEmployeeName.ReadOnly = true;
            this.tbEmployeeName.Size = new System.Drawing.Size(217, 22);
            this.tbEmployeeName.TabIndex = 2;
            // 
            // lNewEmployeeName
            // 
            this.lNewEmployeeName.AutoSize = true;
            this.lNewEmployeeName.Location = new System.Drawing.Point(453, 146);
            this.lNewEmployeeName.Name = "lNewEmployeeName";
            this.lNewEmployeeName.Size = new System.Drawing.Size(45, 17);
            this.lNewEmployeeName.TabIndex = 1;
            this.lNewEmployeeName.Text = "Name";
            this.lNewEmployeeName.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(205, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Surname";
            // 
            // lNewEmployeePos
            // 
            this.lNewEmployeePos.AutoSize = true;
            this.lNewEmployeePos.Location = new System.Drawing.Point(453, 93);
            this.lNewEmployeePos.Name = "lNewEmployeePos";
            this.lNewEmployeePos.Size = new System.Drawing.Size(58, 17);
            this.lNewEmployeePos.TabIndex = 1;
            this.lNewEmployeePos.Text = "Position";
            this.lNewEmployeePos.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Position";
            // 
            // cbName
            // 
            this.cbName.Enabled = false;
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(208, 90);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(144, 24);
            this.cbName.TabIndex = 0;
            this.cbName.SelectedIndexChanged += new System.EventHandler(this.cbName_SelectedIndexChanged);
            // 
            // cbNewEmployeePos
            // 
            this.cbNewEmployeePos.FormattingEnabled = true;
            this.cbNewEmployeePos.Location = new System.Drawing.Point(543, 93);
            this.cbNewEmployeePos.Name = "cbNewEmployeePos";
            this.cbNewEmployeePos.Size = new System.Drawing.Size(217, 24);
            this.cbNewEmployeePos.TabIndex = 0;
            this.cbNewEmployeePos.Visible = false;
            // 
            // cbPosition
            // 
            this.cbPosition.FormattingEnabled = true;
            this.cbPosition.Location = new System.Drawing.Point(48, 90);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new System.Drawing.Size(144, 24);
            this.cbPosition.TabIndex = 0;
            this.cbPosition.SelectedIndexChanged += new System.EventHandler(this.cbPosition_SelectedIndexChanged);
            // 
            // reportsTabPage
            // 
            this.reportsTabPage.Controls.Add(this.dgReports);
            this.reportsTabPage.Controls.Add(this.label11);
            this.reportsTabPage.Controls.Add(this.label14);
            this.reportsTabPage.Controls.Add(this.label10);
            this.reportsTabPage.Controls.Add(this.tbDateEnd);
            this.reportsTabPage.Controls.Add(this.tbBestWorkerDate);
            this.reportsTabPage.Controls.Add(this.tbDateStart);
            this.reportsTabPage.Controls.Add(this.label13);
            this.reportsTabPage.Controls.Add(this.label12);
            this.reportsTabPage.Controls.Add(this.label9);
            this.reportsTabPage.Controls.Add(this.showAvgIngredientsButtons);
            this.reportsTabPage.Controls.Add(this.showUnclaimedIngrButton);
            this.reportsTabPage.Controls.Add(this.showBestWorkerButton);
            this.reportsTabPage.Location = new System.Drawing.Point(4, 25);
            this.reportsTabPage.Name = "reportsTabPage";
            this.reportsTabPage.Size = new System.Drawing.Size(792, 420);
            this.reportsTabPage.TabIndex = 3;
            this.reportsTabPage.Text = "Reports";
            this.reportsTabPage.UseVisualStyleBackColor = true;
            // 
            // dSet
            // 
            this.dSet.DataSetName = "NewDataSet";
            // 
            // showBestWorkerButton
            // 
            this.showBestWorkerButton.Location = new System.Drawing.Point(47, 82);
            this.showBestWorkerButton.Name = "showBestWorkerButton";
            this.showBestWorkerButton.Size = new System.Drawing.Size(75, 23);
            this.showBestWorkerButton.TabIndex = 1;
            this.showBestWorkerButton.Text = "Show";
            this.showBestWorkerButton.UseVisualStyleBackColor = true;
            this.showBestWorkerButton.Click += new System.EventHandler(this.showBestWorkerButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "Show best day workers";
            // 
            // tbDateStart
            // 
            this.tbDateStart.Location = new System.Drawing.Point(292, 21);
            this.tbDateStart.Name = "tbDateStart";
            this.tbDateStart.Size = new System.Drawing.Size(107, 22);
            this.tbDateStart.TabIndex = 3;
            // 
            // tbDateEnd
            // 
            this.tbDateEnd.Location = new System.Drawing.Point(441, 21);
            this.tbDateEnd.Name = "tbDateEnd";
            this.tbDateEnd.Size = new System.Drawing.Size(107, 22);
            this.tbDateEnd.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(289, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Date start";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(438, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 17);
            this.label11.TabIndex = 4;
            this.label11.Text = "Date end";
            // 
            // showUnclaimedIngrButton
            // 
            this.showUnclaimedIngrButton.Location = new System.Drawing.Point(324, 82);
            this.showUnclaimedIngrButton.Name = "showUnclaimedIngrButton";
            this.showUnclaimedIngrButton.Size = new System.Drawing.Size(75, 23);
            this.showUnclaimedIngrButton.TabIndex = 1;
            this.showUnclaimedIngrButton.Text = "Show";
            this.showUnclaimedIngrButton.UseVisualStyleBackColor = true;
            this.showUnclaimedIngrButton.Click += new System.EventHandler(this.showUnclaimedIngrButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(300, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(184, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "Show unclaimed ingredients";
            // 
            // showAvgIngredientsButtons
            // 
            this.showAvgIngredientsButtons.Location = new System.Drawing.Point(553, 82);
            this.showAvgIngredientsButtons.Name = "showAvgIngredientsButtons";
            this.showAvgIngredientsButtons.Size = new System.Drawing.Size(75, 23);
            this.showAvgIngredientsButtons.TabIndex = 1;
            this.showAvgIngredientsButtons.Text = "Show";
            this.showAvgIngredientsButtons.UseVisualStyleBackColor = true;
            this.showAvgIngredientsButtons.Click += new System.EventHandler(this.showAvgIngredientsButtons_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(529, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(203, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Show avg ingedient consuming";
            // 
            // dgReports
            // 
            this.dgReports.DataMember = "";
            this.dgReports.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgReports.Location = new System.Drawing.Point(26, 142);
            this.dgReports.Name = "dgReports";
            this.dgReports.Size = new System.Drawing.Size(731, 260);
            this.dgReports.TabIndex = 5;
            // 
            // tbBestWorkerDate
            // 
            this.tbBestWorkerDate.Location = new System.Drawing.Point(26, 21);
            this.tbBestWorkerDate.Name = "tbBestWorkerDate";
            this.tbBestWorkerDate.Size = new System.Drawing.Size(107, 22);
            this.tbBestWorkerDate.TabIndex = 3;
            this.tbBestWorkerDate.Text = "19.12.2018";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 17);
            this.label14.TabIndex = 4;
            this.label14.Text = "Date";
            // 
            // StaffForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(797, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "StaffForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Make Order";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WaiterForm_FormClosed);
            this.Load += new System.EventHandler(this.StaffForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.makeOrderTab.ResumeLayout(false);
            this.makeOrderTab.PerformLayout();
            this.orderIngredientsTab.ResumeLayout(false);
            this.orderIngredientsTab.PerformLayout();
            this.manageStaffTab.ResumeLayout(false);
            this.manageStaffTab.PerformLayout();
            this.reportsTabPage.ResumeLayout(false);
            this.reportsTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgReports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage makeOrderTab;
        private System.Windows.Forms.Button applyOrder;
        private System.Windows.Forms.TextBox tbTotalPrice;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.ListView lvOrder;
        private System.Windows.Forms.ColumnHeader dishNameOrder;
        private System.Windows.Forms.ColumnHeader dishNumOrder;
        private System.Windows.Forms.ColumnHeader dishPriceOrder;
        private System.Windows.Forms.Button addDish;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.TextBox dishesNum;
        private System.Windows.Forms.Label labelOrder;
        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.ListView lvMenu;
        private System.Windows.Forms.ColumnHeader dishNameMenu;
        private System.Windows.Forms.ColumnHeader dishPriceMenu;
        private System.Windows.Forms.TabPage orderIngredientsTab;
        private System.Windows.Forms.TabPage manageStaffTab;
        private System.Windows.Forms.Button applyIngredientOrder;
        private System.Windows.Forms.ListView lvIngredientOrder;
        private System.Windows.Forms.ColumnHeader orderIngredientName;
        private System.Windows.Forms.ColumnHeader orderIngredientNum;
        private System.Windows.Forms.Button addIngredientToOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIngredientNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lvStock;
        private System.Windows.Forms.ColumnHeader stockIngredientName;
        private System.Windows.Forms.ColumnHeader stockIngredientNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbName;
        private System.Windows.Forms.TabPage reportsTabPage;
        private System.Windows.Forms.TextBox tbEmployeeName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbEmployeeSurname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbEmployeeAge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button dismissButton;
        private System.Windows.Forms.Button promoteButton;
        private System.Windows.Forms.Button demoteButton;
        private System.Windows.Forms.Button newEmployeeButton;
        private System.Windows.Forms.TextBox tbNewEmployeeName;
        private System.Windows.Forms.Label lNewEmployeeAge;
        private System.Windows.Forms.TextBox tbNewEmployeeAge;
        private System.Windows.Forms.Label lNewEmployeeSurname;
        private System.Windows.Forms.TextBox tbNewEmployeeSurname;
        private System.Windows.Forms.Label lNewEmployeeName;
        private System.Windows.Forms.Label lNewEmployeePos;
        private System.Windows.Forms.ComboBox cbNewEmployeePos;
        private System.Windows.Forms.Button applyNewEmployee;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDateEnd;
        private System.Windows.Forms.TextBox tbDateStart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button showAvgIngredientsButtons;
        private System.Windows.Forms.Button showUnclaimedIngrButton;
        private System.Windows.Forms.Button showBestWorkerButton;
        private System.Data.DataSet dSet;
        private System.Windows.Forms.DataGrid dgReports;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbBestWorkerDate;
    }
}